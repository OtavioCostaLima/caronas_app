import React, {Component} from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native'
import ListRide from '../../components/RideList'
import { colors } from '../../styles'
import Icon from 'react-native-vector-icons/FontAwesome'
import Tabs from '../../components/Tabs'

import {
  createStackNavigator,
  createMaterialTopTabNavigator,
} from 'react-navigation';
 

class SearchButtom extends Component {
  render () {
    return ( 
    <TouchableOpacity onPress={ () => this.props.navigation.navigate('SearchRide')}>
      <Icon name="search" size={16} style={{ color: 'white', paddingHorizontal: 15 }} />
    </TouchableOpacity>
      
    );
  }
}

class SecondPage extends Component {
  render() {     
    return (
      <View style={{ flex: 1, backgroundColor: '#CED0D4' }}>
       <ScrollView>
  
           <ListRide navigation={ this.props.navigation } filters= {{ type: 'Ofereço' }}/>
        </ScrollView>
        <Tabs/>
      </View>
    );
  }}


class FirstPage extends Component {
    render() {
      return (
        <View style={{ flex: 1, backgroundColor: '#CED0D4' }}>
         <ScrollView>
             <ListRide navigation={ this.props.navigation } filters= {{ type: 'Procuro' }}/>
          </ScrollView>
          <Tabs/>
        </View>
      );
    }}

const TabScreen = createMaterialTopTabNavigator(
  {
    Procurando: { screen: FirstPage },
    Oferecendo: { screen: SecondPage },
  },
  {
    tabBarPosition: 'top',
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: '#FFFFFF',
      inactiveTintColor: '#F8F8F8',
      style: {
        backgroundColor: colors.primary,
      },
      labelStyle: {
        textAlign: 'center',
      },
      indicatorStyle: {
        borderBottomColor: '#87B56A',
        borderBottomWidth: 2,
      },
    },
  }
);
 
//making a StackNavigator to export as default
const AppNavigator = createStackNavigator({
  TabScreen: {
    screen: TabScreen,
    navigationOptions: ({ navigation }) =>  ( {
      headerRight: <SearchButtom navigation={ navigation } />,
      headerTitleStyle :{textAlign: 'center',alignSelf:'center'},
      headerStyle: {
        backgroundColor: colors.primary,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerTintColor: '#FFFFFF',
      title: 'Caronas',
      alignItems: 'center',
      textAlign: 'center'
    }),
  },
});


export default AppNavigator;


