import React, {Component} from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import { SearchBar } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Icon1 from 'react-native-vector-icons/MaterialIcons'
import styles from './styles'
import Separator from '../../components/Separator'
import { colors } from '../../styles'
import Search from '../../components/Map/Search'
const CurrentLocation = () => (

        <TouchableOpacity style={{ padding: 10 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 10}}>
            <Icon1 style={{ color: colors.primary }} name="gps-fixed" size={ 30 }></Icon1>
            <Text style={{ color: colors.primary, fontWeight: 'bold' , fontSize: 18, paddingLeft: 10 }}>Obter sua Localização atual</Text>
        </View>   
        <Separator/>
        </TouchableOpacity>
)

export default class SearchRide extends Component {

    constructor() {
        super();
    }

    state = {
        search: ''
    };

    updateSearch = search => {
        console.log(search);
        
        this.setState({ search })
    }
    
    componentDidMount()
    {
        this.search.focus()//search is undefined
    }


    handleBackPress = () => {
        this.goBack(); // works best when the goBack is async
        return true;
      }

    render() {
        const { search } = this.state;

        return (
            <View>
                <SearchBar leftIconContainerStyle={ styles.leftIconContainerStyle }
                    searchIcon={ <Icon onPress={() => this.props.navigation.navigate('Ride')} name="arrow-left" size={ 15 } style={ styles.icon } />
                }
                      lightTheme
                      ref={search => this.search = search}
                      inputContainerStyle= { styles.inputContainerStyle }
                      containerStyle = { styles.containerStyle }
                      placeholder="Para onde você quer ir?"
                      onChangeText={this.updateSearch}
                      value={ search }
                />
                  <Search/>

                <View>
                <CurrentLocation/>
                </View>

             </View>
        );
    }
}
