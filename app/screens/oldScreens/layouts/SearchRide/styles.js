import { StyleSheet } from 'react-native'


const styles = new StyleSheet.create({
    inputContainerStyle: {
        backgroundColor: 'white',
        borderWidth: 0,
        margin: 0,
        borderColor: 'white'

    },
    containerStyle: {
        borderWidth: 0,
        backgroundColor: 'white',
        margin: 0,
        borderColor: 'white',
        padding: 0
    },
    icon: {
      fontSize: 20
    },
    leftIconContainerStyle: {
        paddingLeft: 20,
        marginLeft: 0
    }
})

export default styles;