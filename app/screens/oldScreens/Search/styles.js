import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../styles'

const styles = StyleSheet.create({
  
container: { 
    backgroundColor: colors.primary, 
    paddingVertical: 20, 
    justifyContent: 'space-between' 
},
textInput: { 
    borderRadius: 6, 
    marginLeft: 10,
    marginRight: 60, 
    fontSize: 20, 
    marginVertical: 5,
    backgroundColor: colors.white,
    elevation: 5,
    fontFamily: fonts.ralewayLight 
},

button: {
    position:'absolute',
    bottom: 10,
    backgroundColor: colors.primary,
    marginHorizontal: 10,
    borderRadius: 50,
    

  },

  textButton: {
    fontSize: 30,
    fontFamily: fonts.ralewayRegular,
    paddingHorizontal: 10,
    color: colors.white,
    justifyContent: 'flex-end',
    textAlign: 'right'
  }
    
})

export default styles
