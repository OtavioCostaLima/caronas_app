import React, { Component } from 'react'
import { View , BackHandler, TouchableOpacity } from 'react-native'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import utils from '../../../config/utils'
import styles from './styles'
import Button from '../../../components/Buttons/Button'

export default class SearchOrigin extends Component {

  state = {
    localization: null,
    SearchFocused: false,
  }



  handleonLocationSelected = ( data, details ) => {
    this.setState({
      localization: details
    })


  }

    handleBackPress = () => {
        this.props.navigation.navigate('Search')
        return true;
      }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
      }

      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
      }
      

  render() {
    const { SearchFocused, localization } = this.state

    return (

        <View style={{ flex: 1 }}>
        <GooglePlacesAutocomplete 
        styles={ styles }
        enablePoweredByContainer={ false }
        onPress= { (data, details) => this.handleonLocationSelected(data, details) }
        minLength={2}
        placeholder="Pesquisar localização"
        placeholderTextColor="white"
            query= {{
              key: utils.googleApiKey,
              language: 'pt',
            }}
            textInputProps={{
          onFocus: () => { this.setState({ SearchFocused: true }) },
          onBlur: () => { this.setState({ SearchFocused: false }) },
          autoCapitalize: "none",
          autoCorrect: false
      }}
      listViewDisplayed= { SearchFocused }
      nearbyPlacesAPI={'GoogleReverseGeocoding'}
      debounce={200}
      GooglePlacesSearchQuery={{
             // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
             rankby: 'distance'
           }}
            currentLocation={true}
            currentLocationLabel="Minha Localização atual"
            fetchDetails={true}
        />
         
            { localization && 
            <Button style={{ marginHorizontal: 20, bottom: 20 }} 
            onPress={ () => { this.props.navigation.navigate('Search', 
            {localizationDestination: localization}) }}
            localization={ localization } >
              Avançar
            </Button> }
        </View>
    
    )
  }
}

