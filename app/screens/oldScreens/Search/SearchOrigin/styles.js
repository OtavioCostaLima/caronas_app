import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../../styles'

const styles = new StyleSheet.create({
    container: {
      backgroundColor: colors.background,
    },

    textInputContainer: {
      backgroundColor: colors.background,
      height: 80,
      marginBottom: 20
    },

    textInput: {
      fontSize: 20,
      borderWidth: 0,
      marginTop: 20,
      marginBottom: 0,
      marginHorizontal: 10,
      height: 80,
      marginHorizontal: 0,
      borderRadius: 6, 
      fontFamily: fonts.ralewayBold, 
      marginVertical: 5,
      elevation: 5,
      backgroundColor: colors.primary,
      borderColor: colors.primary,
      color: colors.white,
    },


    listView: {
    },
    separator: {
  
    },
    row:{
  
    },
  
    description:{
      fontSize: 16,
      fontFamily: fonts.ralewayRegular,
      
    },

    predefinedPlacesDescription:{

    },

    buttonNext: {
      bottom: 10,
      backgroundColor: colors.primary,
      marginHorizontal: 10,
      borderRadius: 50,
      

    },

    textButtonNext: {
      fontSize: 30,
      fontFamily: fonts.ralewayRegular,
      paddingHorizontal: 10,
      color: colors.white,
      justifyContent: 'flex-end',
      textAlign: 'right'
    }
  
  })
  
  export default styles