import React, { Component } from 'react'
import { View , BackHandler } from 'react-native'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import utils from '../../../config/utils'
import styles from './styles'
import Button from '../../../components/Buttons/Button'
import { colors } from '../../../styles'

export default class SearchOrigin extends Component {
  static navigationOptions = {
    title: 'Detalhes da viagem',
    headerStyle: {
        backgroundColor: colors.primary,
      },    
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
  };
  
  state = {
    localization: null,
    SearchFocused: false,
  }

  handleonLocationSelected = ( data, details ) => {
    this.setState({
      localization: details
    })
  }

    handleBackPress = () => {
        this.props.navigation.goBack()
        return true;
      }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
      }

      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
      }
      
      goBack() {
        const { navigation } = this.props;
        navigation.goBack();
        navigation.setParams({localizationOrigin: this.state.localization});
      }

  render() {
    const { SearchFocused, localization } = this.state

    return (

        <View style={{ flex: 1 }}>
        <GooglePlacesAutocomplete 
        styles={ styles }
        enablePoweredByContainer={ false }
        onPress= { (data, details) => this.handleonLocationSelected(data, details) }
        minLength={2}
        placeholderTextColor="white"
        textInputProps={{
          onFocus: () => { this.setState({ SearchFocused: true }) },
          onBlur: () => { this.setState({ SearchFocused: false }) },
          autoCapitalize: "none",
          autoCorrect: false
      }}
      listViewDisplayed= { SearchFocused }
      nearbyPlacesAPI={'GoogleReverseGeocoding'}
      debounce={200}
      GooglePlacesSearchQuery={{
             // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
             rankby: 'distance'
           }}
        placeholder="Pesquisar localização"
            query= {{
              key: utils.googleApiKey,
              language: 'pt',
            }}
            currentLocation={true}
            currentLocationLabel="Minha Localização atual"
            fetchDetails={true}
        />
         
         { localization && 
            <Button style={{ marginHorizontal: 20, bottom: 20 }} 
            onPress={ () => this.goBack() }
            localization={ localization } >
              Avançar
            </Button> }

        </View>
    
    )
  }
}

