import React, { Component } from 'react'
import { Text, View, FlatList, DatePickerAndroid, TimePickerAndroid, TextInput } from 'react-native'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Button from '../../components/Buttons/Button'
import Input from '../../components/TextInput'
import DateInput from '../../components/DateInput'

import { colors } from '../../styles';

class RecentRides extends Component {
  render() {
    return (
    <FlatList
      data={[{key: 'a'}, {key: 'b'}]}
      renderItem={({item}) => <Text>{item.key}</Text>}
    />
    )
  }
}



export default class Search extends Component {
    
  constructor(props) {
    super(props)

  }
  state = {
    startDate: null,
    backDate: null,
  }

    setDateStart = (dateStart) => {
      this.setState({
        startDate: dateStart
      })
    }

    setBackDate = (backDate) => {
      this.setState({
        backDate: backDate
      })
    }


      toNavigate = routeName => {
        this.props.navigation.navigate(routeName)
      }

     

      async getTime () {
        try {
          const {action, hour, minute} = await TimePickerAndroid.open({
            hour: 14,
            minute: 0,
            is24Hour: false, // Will display '2 PM',
            mode: 'spinner'
          });
          if (action !== TimePickerAndroid.dismissedAction) {

            // Selected hour (0-23), minute (0-59)
          }
        } catch ({code, message}) {
          console.warn('Cannot open time picker', message);
        }
      }

  render() {

    const { localizationOrigin, localizationDestination } = this.props.navigation.state.params || {};
    return (
      <View style={{ flex: 1}}>
        <View>
          <Icon name="compare-arrows" style={{ position:'absolute', right:10, top: 70, color: colors.primary, transform: [ {rotate: '90deg'}] }} size={ 40 }/>
          <Text style={{ textAlign: 'center' }}> Rotas </Text>

          <Input  onFocus={ () => this.toNavigate('SearchOrigin') } 

          style={ styles.textInput } placeholder="Origem" value={ localizationOrigin ? localizationOrigin.formatted_address : '' }/>
          
          <Input onFocus={ () => this.toNavigate('SearchDestination') } 
          style={ styles.textInput } placeholder="Destino" value={ localizationDestination ? localizationDestination.formatted_address : ''} />
      </View>

        
      <DateInput leftIcon="date-range" handleFocus={ this.setDateStart } placeholder="Data ida" />

      <DateInput leftIcon="date-range" handleFocus={ this.setBackDate } placeholder="Data volta" />


        <Text>Item</Text>
      <RecentRides/>

        <Button onPress= { () => alert(this.state.startDate) } style={{ marginHorizontal: 20, bottom: 20 }}>
              <Text>Próximo</Text>
        </Button>

      </View>

    )
  }
}
