import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Button from '../../components/Buttons/Button'
import { colors } from '../../styles'

export default class PainelVacancies extends Component {
    static navigationOptions = {
        title: 'Detalhes da viagem',
        headerStyle: {
            backgroundColor: colors.primary,
          },    
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
      };

    state = {
        countvalue: 1
    }

    addVacance = () => {
        if(this.state.countvalue < 4){
            this.setState({
                countvalue: this.state.countvalue +1
            })
        }
    }

    removeVacance = () => {
        if(this.state.countvalue > 1) {
            this.setState({
                countvalue: this.state.countvalue-1
            })
        }

    }
 
  render() {

    const { countvalue } = this.state

    return (
    
        <View style={ styles.container }>
        <Text style={ styles.message }>Informe a quantidade de vagas</Text>
    
      <View style={styles.containerCounter}>
        <TouchableOpacity onPress={ this.removeVacance }>
        <Icon style={ styles.buttomDec } name="remove-circle-outline" size={60} color={ colors.primary }/>
        </TouchableOpacity>
        <Text style= { styles.countValue }> { countvalue } </Text>

        <TouchableOpacity onPress={ this.addVacance }>
        <Icon style={ styles.buttomInc } name="add-circle-outline" size={60} color={ colors.primary }/>
        </TouchableOpacity>
      </View>
      <Button style={ styles.buttonNext }>Avançar</Button>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: 'center',

    },
    containerCounter: {
    width: '100%',
    top: '60%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  countValue: {
    fontSize: 30,
  },
  buttomInc: {
    right: 20
  },

  buttomDec: {
    left: 20
  },

  message: {
      top: '20%',
      fontSize: 25,

  },
  buttonNext: {
    bottom: 20,
  }
  
})
