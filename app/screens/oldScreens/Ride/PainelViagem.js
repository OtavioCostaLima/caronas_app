import React , { Component } from 'react'
import { View, Text } from 'react-native'
import { colors } from '../../styles'
import TimePicker from '../../components/TimePicker'
import DatePicker from '../../components/DateInput'
import { CheckBox } from 'react-native-elements'
import Button from '../../components/Buttons/Button'
import Input from '../../components/TextInput'
import styles from './styles'


export default class PainelViagens extends Component {
    /* driver, passenger, type, origin, destination, stops, dateOut, returnDate,
    exitTime, backDate, returnTime, availableVacancies, image, amount, createdAt, updatedAt
    */

    state = {
        checked: false,
        ride: {
            origin: null,
            dateOut: null,
            returnDate: null,
        },
        exitTime: null,
        returnTime: null

    }

    static navigationOptions = {
        title: 'Detalhes da viagem',
        headerStyle: {
            backgroundColor: colors.primary,
          },    
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
      };

      setExitTime = time => {
        this.setState({
            exitTime: time
        })            
      }

      setReturnTime = time => {
        this.setState({
            returnTime: time
        })
      }
       setDateOut = date => {
        this.setState({
            exitTime: date
         })            
      }
    
        setReturnDate = date => {
            this.setState({
                returnTime: date
         })
        }

        toNavigate = routeName => {
            this.props.navigation.navigate(routeName)
          }

    render() {
        const { localizationOrigin, localizationDestination } = this.props.navigation.state.params || {};

        return(
            <View style={{ flex: 1 }}>
                <Input  onFocus={ () => this.toNavigate('SearchOrigin') } 
                style={ styles.textInput } placeholder="Origem" 
                value={ localizationOrigin ? localizationOrigin.formatted_address : '' }
                />

                <Input onFocus={ () => this.toNavigate('SearchDestination') } 
          style={ styles.textInput } placeholder="Destino" 
        value={ localizationDestination ? localizationDestination.formatted_address : ''} 
          />

            <DatePicker placeholder="Data saida" handleClickDatePicker={this.setExitTime}/>
            <TimePicker  placeholder="Hora saída" handleClickTimePicker={this.setDateOut}/>
            <CheckBox title='Vai voltar?' checked={this.state.checked} 
            onPress={() => this.setState({checked: !this.state.checked})}/>

            { this.state.checked && 
            <View>
                <DatePicker placeholder="Data volta" handleClickDatePicker={this.setReturnDate}/>
                <TimePicker  placeholder="Hora volta" handleClickTimePicker={ this.setReturnTime}/>
            </View>
            }
            <Text>{localizationOrigin}          </Text>    
            <Button style={{ bottom: 10, marginHorizontal: 20 }}>Avançar</Button>

            </View>
        );
    }
}