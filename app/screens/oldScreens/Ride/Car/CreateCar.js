import React, { Component } from 'react'
import { Text, View, TextInput, StyleSheet } from 'react-native'
import Wizard from '../../../components/WizardStep'

export default class CreateCar extends Component {

render() {
    return (
    <View style={ styles.root }>
      <Wizard initialValues={{ username: '', email: '', password: ''  }}>

        <Wizard.Step>
        {({ onChangeValue, values }) => (
            <View style={styles.container}>
                <TextInput value={ values.username } placeholder="User name" onChangeText={ text => onChangeValue('username', text) } />
            </View>
        )}
        </Wizard.Step>
        <Wizard.Step>
        {({ onChangeValue, values }) => (
            <View style={styles.container}>
                <TextInput value={ values.email } placeholder="email" onChangeText={ text => onChangeValue('email', text) }/>
            </View>
        )}
        </Wizard.Step>
        <Wizard.Step>
        {({ onChangeValue, values }) => (
            <View style={styles.container}>
                <TextInput value={ values.password } placeholder="password" onChangeText={ text => onChangeValue('password', text) } />
            </View>
        )}
        </Wizard.Step>
      </Wizard>
    </View>
    )
  }
}


const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  container: { 
      flex: 1, 
      alignItems: 'center', 
      justifyContent: 'center' 
    }
})
