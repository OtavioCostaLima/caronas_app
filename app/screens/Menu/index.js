import React, { Component } from 'react'
import { SafeAreaView, Text, View, FlatList, TouchableOpacity, TouchableNativeFeedback, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { colors } from '../../styles'

const menus = [{
    name: 'Perfil',
    icon: 'person',
    route: 'Profile'
},
{
    name: 'Conta',
    icon: 'account-circle',
},
{
    name: 'Suas Viagens',
    icon: 'card-travel'
},
{
    name: 'Forma de Pagamento',
    icon: 'payment'
},
{
    name: 'Chat',
    icon: 'chat'
},
{
    name: 'Favoritos',
    icon: 'favorite'
},
{
    name: 'Configuração',
    icon: 'settings'
},
{
    name: 'Sobre',
    icon: 'info',
    route: 'About'
},
{
    name: 'Ajuda',
    icon: 'help',
    route: 'Help'
},
{
    name: 'Sair',
    icon: 'exit-to-app'
}
]

const ItemMenu = ({ children, style, icon, onPress }) => {
    return (
        <TouchableNativeFeedback
            onPress={ onPress }
            background={ TouchableNativeFeedback.Ripple('#DCD3D1', false) }>
        <View style={ [styles.item, style] }>
            <Icon style={ [ styles.icon, style] } name={icon} size={40}/>
            <Text style={[ styles.text, style ]}>{children}</Text>
        </View>                    
        </TouchableNativeFeedback>
        )}

export default class index extends Component {
    static navigationOptions = {
      title: 'Menu',
      headerStyle: {
          elevation: 0,       //remove shadow on Android
          shadowOpacity: 0,   //remove shadow on iOS
          }
    }

    createRows = (data, columns) => {
        const rows = Math.floor(data.length / columns); // [A]
        let lastRowElements = data.length - rows * columns; // [B]
        while (lastRowElements !== columns) { // [C]
          data.push({ // [D]
            id: `empty-${lastRowElements}`,
            name: `empty-${lastRowElements}`,
            empty: true
          });
          lastRowElements += 1; // [E]
        }
        console.log(data);
        
        return data; // [F]
      }


  render() {
      const columns = 3;
      const {navigation} = this.props; 
    return (
      <SafeAreaView style={ styles.container }>
      <FlatList
        numColumns={columns}
        data={ this.createRows(menus, columns) }
        renderItem={  ({item}) =>  
        {  if (item.empty) {
         return <ItemMenu  style={styles.itemEmpty}>{ item.name }</ItemMenu>
        }   
        return <ItemMenu onPress={ () => navigation.push(item.route) } icon={item.icon} >{ item.name }</ItemMenu>
        
    }}
        /> 
        
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
      paddingHorizontal: 5
   //   flex: 1,
  // backgroundColor: "#D9D9D9"
  },
  item: {
   // paddingHorizontal: 40,
   flexBasis: 0, // evitar o overflow
   flexGrow: 1,
    margin:4,
   backgroundColor: colors.defaultGrey,
   padding: 20,
   alignItems: "center",
   elevation: 0,
   justifyContent: 'center',
   borderRadius: 3,
},
itemEmpty: {
    backgroundColor: 'transparent',
    elevation: 0,
    color: 'transparent',
},
  text: {
    color: colors.primary,
    textAlign: 'center'
  },
  icon: {
    color: colors.primary,
  }
})

