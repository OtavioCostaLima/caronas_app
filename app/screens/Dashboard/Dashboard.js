import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity,TouchableWithoutFeedback, Image, Modal, Alert } from 'react-native'
import Car from 'react-native-vector-icons/MaterialIcons'
import Plus from 'react-native-vector-icons/MaterialCommunityIcons'
import UserIcon from 'react-native-vector-icons/FontAwesome'
import { fonts } from '../../styles'

const CircleButton = ({ handleClickButton }) => {
  return (
    <TouchableOpacity
      onPress={handleClickButton}
      style={[styles.itemMenu, styles.circleButton]}>
      <Plus name="plus" size={30} color="#fff" />
    </TouchableOpacity>
  )
}

const TravelButton = ({ style, title, handleClickButton }) => (
   <View style={styles.buttonContainer}>
      <TouchableWithoutFeedback
       onPress={ handleClickButton }
      >
          <Text style={ style }>{ title }</Text>
      </TouchableWithoutFeedback>
      </View>
)

const CardTravel = () => {

  return (
    <View style={styles.cardTravel}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View>
          <Text>12:00 > Floriano - PI</Text>
          <Text>19:00 > Teresina - PI</Text>
        </View>
        <Text>20,00</Text>
      </View>
      <Text>Alexandro Vieira</Text>
      <Text>Motorista</Text>

      <Text>2 vagas</Text>
    </View>
  )
}

const BottomBarMenu = ({ handleClickButton, navigation }) => {

  return (
    <View style={styles.BottomBarMenu}>
      <View style={styles.menuItemContainer}>
        <TouchableOpacity
          style={[styles.itemMenu, styles.selected]}>
          <Car name="menu" size={30} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.itemMenu}>
          <Car name="directions-car" size={30} />
        </TouchableOpacity>
        <CircleButton handleClickButton={handleClickButton} />
        <TouchableOpacity style={styles.itemMenu}>
          <Car name="chat" size={30} />
        </TouchableOpacity>
        <TouchableOpacity 
        onPress={ () => navigation.navigate('Profile') }
        style={styles.itemMenu}>
          <UserIcon name="user" size={30} />

        </TouchableOpacity>

      </View>

    </View>
  )
}

export default class Dashboard extends Component {

  state = {
    modalVisible: false
  }

  static navigationOptions = {
    title: 'Caronas'
  }


  render() {

    const {navigate} = this.props.navigation;

    return (
      <View style={styles.container}>
        <Modal 
        transparent
          animationType="slide"
          onRequestClose={() => { Alert.alert('Modal has been closed.'); }}
          visible={this.state.modalVisible}>
          <View
            onTouchEnd={ () => this.setState({ modalVisible: false }) }
           style={{ flex:1, backgroundColor: '#000', opacity: 0.5 }}>
          </View>
          <View style={{ height: 180, backgroundColor: '#fff' }}> 
              <TouchableOpacity onPress={() => this.setState({ modalVisible: false })}>
                <Text>Close</Text>
              </TouchableOpacity>
              <TravelButton handleClickButton={ () => navigate('RideStepFirst') } style={ styles.buttonInlime } title="PROCURAR CARONA"/>
              <TravelButton handleClickButton={ () => {} }  style={ styles.buttonOutlime } title="OFERECER CARONA"/>
            </View>
        </Modal>
        <CardTravel />
        <BottomBarMenu handleClickButton={ () => this.setState({ modalVisible: true }) } navigation={ this.props.navigation } />

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  BottomBarMenu: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingHorizontal: 10,
    elevation: 5,
    backgroundColor: 'white',
    shadowOffset: { width: 10, height: 10, },
    shadowColor: 'black',
    shadowOpacity: 1.0,
    shadowRadius: 2,
    height: 50
  },
  itemMenu: {
    padding: 10
  },
  menuItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  circleButton: {
    borderRadius: 50,
    backgroundColor: '#5C2173',
    elevation: 4,
    bottom: 35,
    padding: 20,
    shadowOffset: { width: 10, height: 10, },
    shadowColor: 'black',
    shadowOpacity: 1.0,
    shadowRadius: 2,
  },

  selected: {
    borderTopWidth: 3,
    borderTopColor: '#5C2173'
  },

  // card travel
  cardTravel: {
    borderRadius: 8,
    elevation: 5,
    backgroundColor: 'white',
    marginHorizontal: 10,
    padding: 10
  },
  buttonInlime: {
    paddingVertical: 20,
    color: '#fff',
    fontFamily: fonts.ralewayBold,
    fontSize: 18,
    textAlign: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
    backgroundColor: '#5C2173',
    borderWidth: 2,
    borderColor: '#5C2173',
    alignItems: 'center',
    justifyContent: 'center',

  },

  buttonOutlime: {
    paddingVertical: 20,
    color: '#5C2173',
    backgroundColor: '#fff',
    fontFamily: fonts.ralewayBold,
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 20,
    borderWidth: 2,
    borderColor: '#5C2173',
    justifyContent: 'center',

  },

  buttonContainer: {
    marginVertical: 5,
  }
})

