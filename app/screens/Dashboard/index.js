import React, { Component } from 'react'
import { Text, View, Button, TouchableWithoutFeedback } from 'react-native'
import Icon from  'react-native-vector-icons/MaterialIcons'
import { colors } from '../../styles'

export default class index extends Component {
    static navigationOptions =( { navigation}) => {
        return {
        //  title: 'Perfil',
          headerTitleStyle: { textAlign: 'center', flex: 1 },
          headerStyle:{
            elevation: 0,
            shadowOpacity: 0,
            marginRight: 20,
            paddingTop: 20,
          },
          headerRight: (
              <TouchableWithoutFeedback onPress={() => navigation.push('Menu')} >
                  <Icon name="menu" size={ 40 } color={colors.primary}/>
              </TouchableWithoutFeedback> 
              )}
        }

  render() {
    return (
      <View>
        <Text> Dashboard </Text>
      </View>
    )
  }
}
