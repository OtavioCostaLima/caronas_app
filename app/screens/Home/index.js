import React, { Component } from 'react'
import { Text, View, Image, TouchableNativeFeedback, ImageBackground } from 'react-native'
import styles from './styles'

export default class Login extends Component {
  render() {
    
    return (

     <View style={ styles.container }>
       <ImageBackground source={ require('../../images/background.jpg') } style={{width: '100%', height: '100%'}}>

  </ImageBackground>
        <Image style={ styles.logo } source={require('../../images/logo.png')} />

    
       <Text style={ styles.message}> Viage de qualquer lugar! </Text>

       <View style={ [styles.buttonProcurar, styles.button ] }>
                  <TouchableNativeFeedback 
                  onPress={ () => this.props.navigation.navigate('Search') }
                  style={styles.touchable}
                  background={TouchableNativeFeedback.Ripple('#003194', true)}  >
                      <View  >
                          <Text  style={ [styles.TextButtonProcurar, styles.textButton] } >PROCURAR CARONA</Text>
                      </View>
                  </TouchableNativeFeedback>
          </View>

        <View style={ [styles.buttonOferecer, styles.button ] }>
                <TouchableNativeFeedback 
                onPress={ () => this.props.navigation.navigate('Search') }
                background={TouchableNativeFeedback.Ripple('#C0C0C0', true)}>
                    <View>
                        <Text  style={ [styles.TextButtonOferecer, styles.textButton] } >OFERECER CARONA</Text>
                    </View>
                </TouchableNativeFeedback>
        </View>

      </View>
    )
  }
}
