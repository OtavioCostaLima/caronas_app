import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../styles'

const styles = new StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    logo: {
        position: 'absolute',
        top: 0,
        left: 10,
        height: 80,
        width: 80
    },

    touchable: {
        borderRadius: 28,
        overflow: 'hidden',
      },
    
    buttonProcurar: {
        bottom: 140,
        backgroundColor: colors.primary,
     },

    buttonOferecer: {
        bottom: 80,
        backgroundColor: colors.white,
        borderColor: colors.primary,
    },

    button: {
        position: 'absolute',
        borderRadius: 50,
        width: "90%",
        justifyContent: 'center',
        textAlign: 'center',
        elevation: 5,
        shadowColor: '#000',
        shadowOpacity: 0.1,
        shadowOffset: { x: 0, y:0 },
        shadowRadius: 15,

    },

    TextButtonProcurar: {
        color: colors.white,
    },

    TextButtonOferecer: {
        color: colors.regular,
    },

    textButton: {
        fontFamily: fonts.ralewayBold,
        textAlign: 'center',
        paddingVertical: 10,
        fontSize: 20,
    },

    register: {
        color: colors.primary,
        position: 'absolute',
        bottom: 20,
        fontSize: 18,
        fontFamily: fonts.ralewayBold,
    },

    message: {
        position: 'absolute',
        top: "30%",
        fontSize: 40,
        justifyContent: 'center',
        textAlign: 'center',
        fontFamily: fonts.ralewayExtrabold,
        color: colors.white,
 

    },



})

export default styles;