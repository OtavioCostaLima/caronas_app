import React, { Component } from 'react'
import {
  Text, View, TextInput, TouchableWithoutFeedback, TouchableOpacity,
  StyleSheet, Image, Modal, ScrollView, FlatList, AsyncStorage
} from 'react-native'
import { fonts } from '../../../styles'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Separator from '../../../components/Separator'
import { contries } from './condes'
import { LoginManager, AccessToken,   GraphRequest,
  GraphRequestManager } from 'react-native-fbsdk';
import axios from 'axios';

export class LoginByCodeFirstStep extends Component {

  state = {
    modalVisible: false,
    contrieSelected: {
      "name": "Brazil",
      "dial_code": "+55",
      "code": "BR"
    },
  }

  _storeData = async (user) => {
    try {
      console.log(user);
      await AsyncStorage.setItem('userId', user._id);
      await AsyncStorage.setItem('userName', user.name);
      await AsyncStorage.setItem('facebookId', user.facebookId);
      await AsyncStorage.setItem('imagemProfile', user.imagemProfile);
    } catch (error) {
      console.log("ERROR: ", error);

    }
  }

  _renderItem = item => (
    <View>
          <Separator/>

      <TouchableOpacity onPress={() =>
        this.setState({ contrieSelected: item, modalVisible: false })}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{
            paddingVertical: 20, 
            paddingHorizontal: 10, 
            fontSize: 18,
            width: 90, 
            backgroundColor: '#5C2173', 
            textAlign: 'center', 
            color: '#FFF'}}>
            {item.dial_code}
            </Text>
          <Text style={{ 
            paddingVertical: 20, 
            paddingHorizontal: 10, 
            fontSize: 18 
            }}>{item.name} </Text>
        </View>
      </TouchableOpacity>
    </View>
  )

  loginGoogle = async () => {
    this.props.navigation.navigate('MenuNavigation')
  }

  loginByFacebook = async () => {
    console.log("entrou");
    
    let result;
    try {
      LoginManager.setLoginBehavior('NATIVE_ONLY');
      result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
    } catch (nativeError) {
      await  console.log("Error login native");
      try {
        console.log("try WEB_ONLY");
        LoginManager.setLoginBehavior('WEB_ONLY');
        result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
      } catch (webError) {
        console.log("error web only");
        
      }
    }
    if (result.isCancelled) {
      this.setState({
         showLoadingModal: false,
       // notificationMessage: I18n.t('welcome.FACEBOOK_CANCEL_LOGIN')
      });
    } else {
      // Create a graph request asking for user information
      this.FBGraphRequest('id,email', this.FBLoginCallback);
    }
 
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        console.log('AccessToken data', data.accessToken)
        axios.get(`http://192.168.0.7:3000/users/auth/facebook/token?access_token=${data.accessToken}`)
        .then(response => {
          
          let data = response.data;
          this._storeData(data)
          this._retrieveData()          
         

          
        })
      }
    )
    this.props.navigation.navigate('MenuNavigation')
  
  }




FBGraphRequest = async (fields, callback) => {
 // const accessData = await AccessToken.getCurrentAccessToken();

  const infoRequest = new GraphRequest('/me?fields=name,picture,email', null, callback.bind(this));

  new GraphRequestManager().addRequest(infoRequest).start();
}

 FBLoginCallback = async (error, result) => {
  if (error) {
    console.log("FBLoginCallback: ", error);
    
    this.setState({
//      showLoadingModal: false,
   //   notificationMessage: I18n.t(‘welcome.FACEBOOK_GRAPH_REQUEST_ERROR’)
    });
  } else {
      console.log(result);
    // Retrieve and save user details in state. In our case with 
    // Redux and custom action saveUser
   // this.props.saveUser({
  //    id: result.id,
  //    email: result.email,
  //    image: result.picture.data.url
  //  });
  }
}

  render() {

    const { contrieSelected } = this.state

    

    return (
      <View style={styles.container}>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => this.setState({ modalVisible: false })}>
          <View style={{ top: 10 }}>
            <TouchableWithoutFeedback
              onPress={() => this.setState({ modalVisible: false })}>
              <View style={styles.closeButtom}>
                <Icon name="close" size={40} />
              </View>
            </TouchableWithoutFeedback>

            <ScrollView>
              <FlatList
                data={contries}
                renderItem={({ item }) => this._renderItem(item)}
              />
            </ScrollView>
          </View>
        </Modal>

        <Text style={styles.title}>Qual é seu numero</Text>
        <Text style={[styles.title, styles['title-primary']]}> de telefone?</Text>

        <View style={styles.inputContainer}>
          <TouchableWithoutFeedback onPress={() => this.setState({ modalVisible: true })}>
            <Text style={styles.textInputCode}>{contrieSelected.dial_code}</Text>
          </TouchableWithoutFeedback>
          <TextInput keyboardType="numeric" style={styles.textInputPhone} placeholder="89 9 99268417" />
        </View>

        <View style={styles.buttonContainer}>
          <TouchableWithoutFeedback>
            <Text style={styles.TextButton}>Enviar código</Text>
          </TouchableWithoutFeedback>
        </View>

        <Text style={{ flexDirection: 'row', alignSelf: 'center', top: 230, fontSize: 18 }}>ou</Text>

        <View style={styles.socialContainer}>
          <TouchableWithoutFeedback 
          onPress={ () => { this.loginByFacebook() } }
          >
            <View style={[styles['button-facebbok'], styles['social-button-container']]}>
              <Image style={styles['image-facebbok']}
                source={require('../../../images/facebook.png')} />
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback 
          onPress={ () => this.loginGoogle()  }>
            <View style={[styles['button-google'], styles['social-button-container']]}>
              <Image style={styles['image-google']}
                source={require('../../../images/google.png')} />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}

export default LoginByCodeFirstStep

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonContainer: {
    top: 150
  },
  TextButton: {
    paddingVertical: 20,
    backgroundColor: '#5C2173',
    color: '#fff',
    fontFamily: fonts.ralewayBold,
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 20,


  },

  'social-button-container': {
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10
  },

  'button-facebbok': {
    height: 44,
    width: 97,
    backgroundColor: '#7096DF',
  },

  'button-google': {
    height: 44,
    width: 97,
    backgroundColor: '#F1F1F1',
  },
  'image-google': {
    height: 25,
    width: 25,
  },

  'image-facebbok': {
    height: 27,
    width: 12,
    backgroundColor: '#7096DF',
  },

  inputContainer: {
    //  backgroundColor: 'red',
    flexDirection: 'row',
    alignSelf: 'center',
    top: 120,
    width: '90%',
  },

  textInputCode: {
    fontSize: 18,
    //backgroundColor: '#7096DF',
    textAlign: 'center',
    alignContent: 'center',
    textAlignVertical: "center",
    marginRight: 10,
    padding: 10,
    borderBottomWidth: 2
  },

  textInputPhone: {
    fontSize: 25,
    backgroundColor: '#F1F1F1',
    flex: 6,
    paddingHorizontal: 10
  },

  inputModal: {
    fontSize: 25,
    backgroundColor: '#F0F2F1',
  },

  title: {
    fontFamily: fonts.ralewayRegular,
    fontSize: 35,
    alignSelf: 'center',
    top: 40
  },
  'title-primary': {
    color: '#5C2173',
  },

  socialContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    bottom: -300,
  },

  closeButtom: {
    alignSelf: 'flex-end',
    marginRight: 10,
    marginBottom: 20,
  }




})
