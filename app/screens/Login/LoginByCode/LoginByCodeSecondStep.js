import React, { Component } from 'react'
import {
  Text, View, TextInput, TouchableWithoutFeedback, StyleSheet, TouchableOpacity } from 'react-native'
import { fonts } from '../../../styles'

const ConfirmButton = ({ validate = false }) => {
    return( validate? 
     <View style={styles.buttonContainer}>
        <TouchableWithoutFeedback
         onPress={ () => alert('confirm') }
        >
            <Text style={[styles.TextButton, styles.buttonEnabled  ]}>Confirmar</Text>
        </TouchableWithoutFeedback>
        </View>
        :       
        <View style={styles.buttonContainer}>
        <TouchableWithoutFeedback>
            <Text style={[styles.TextButton, styles.buttonDisabled  ]}>Confirmar</Text>
        </TouchableWithoutFeedback>
        </View>
          )
}

export class LoginByCodeSecondStep extends Component {

    constructor(props){
        super(props)
          this.state = {
    code1: null,
    code2: null,
    code3: null,
    code4: null,
    codeValidate: false
  }
    }




  componentDidMount()
  {
      this.field1.focus()//search is undefined
  }

  goToField2 = async  text => {
      await this.setState({ code1: text })
      if(text.length == 1){
        this.field2.focus()//search is undefined
      }
      this._validate()
  }

  goToField3 = async text => {
    await this.setState({ code2: text })
    if(text.length == 1){
        this.field3.focus()//search is undefined
    }
    this._validate()
  }

  goToField4 = async text => {
    await this.setState({ code3: text })
    if(text.length == 1){
        this.field4.focus()//search is undefined
    }
    this._validate()
  }

  getCode4 = async text => {
    await this.setState({ code4: text })
    this._validate()
  }

  _validate = () => {
    const { code1, code2, code3, code4 } = this.state
    if(code1 && code2 && code3 && code4){
    return this.setState({ codeValidate: true })    
} 
    this.setState({ codeValidate: false })    
  }

  render() {


    return (

      <View style={styles.container}>
        <Text style={styles.title}>Enviamos um código</Text>
        <Text style={[styles.title, styles['title-primary']]}>para seu telefone.</Text>

        <TouchableOpacity>
            <Text style={{ color: '#F5A522', alignSelf: 'center', top: 230 }}>Reenviar código.</Text>
        </TouchableOpacity>


        <View style={styles.inputContainer}>
          <TextInput 
            ref={field1 => this.field1 = field1}
           onChangeText={ text => this.goToField2(text) }
           maxLength={ 1 }
          keyboardType="numeric" style={styles.textInputPhone} placeholder="" />
          <TextInput 
          maxLength={ 1 }
          ref={field2 => this.field2 = field2}
          onChangeText={ text => this.goToField3(text) }

          keyboardType="numeric" style={styles.textInputPhone} placeholder="" />
          <TextInput 
            maxLength={ 1 }
                    ref={field3 => this.field3 = field3}
                    onChangeText={ text => this.goToField4(text) }
          keyboardType="numeric" style={styles.textInputPhone} placeholder="" />
          <TextInput 
                    maxLength={ 1 }

         ref={field4 => this.field4 = field4}
          keyboardType="numeric" style={styles.textInputPhone} placeholder="" 
          onChangeText={ text => this.getCode4(text) }
          />
        </View>

        <ConfirmButton validate={ this.state.codeValidate }/>


<View style={ styles.terms }>
        <Text>Ao confirmar você está aceitando</Text>
        <Text>os termos de uso da aplicação.</Text>
</View>


      </View>
    )
  }
}

export default LoginByCodeSecondStep

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonContainer: {
    top: 200
  },
  TextButton: {
    paddingVertical: 20,
    color: '#fff',
    fontFamily: fonts.ralewayBold,
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 20,
  },

  buttonDisabled: {
    backgroundColor: '#9F92AD'
  },

  buttonEnabled: {
    backgroundColor: '#5C2173',
  },

  inputContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignSelf: 'center',
    top: 120,
    width: '90%',
  },

  textInputPhone: {
    fontSize: 25,
    backgroundColor: '#F0F2F1',
    textAlign: 'center',
    paddingHorizontal: 25,
    paddingVertical: 15,
    },

  title: {
    fontFamily: fonts.ralewayRegular,
    fontSize: 35,
    alignSelf: 'center',
    top: 40
  },
  'title-primary': {
    color: '#5C2173',
  },

  terms: {
    top: 400,
    alignSelf: 'center',
    fontSize: 4,
    fontFamily: fonts.ralewayRegular
  }

})
