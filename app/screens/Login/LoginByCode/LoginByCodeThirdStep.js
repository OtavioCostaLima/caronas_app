import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableWithoutFeedback, TextInput, SafeAreaView } from 'react-native'
import { fonts } from '../../../styles'
import axios from 'axios'

const ConfirmButton = ({ validate = false, submitForm }) => {
    return( validate? 
     <View style={styles.buttonContainer}>
        <TouchableWithoutFeedback
         onPress={ () => submitForm }
        >
            <Text style={[styles.TextButton, styles.buttonEnabled  ]}>Continuar</Text>
        </TouchableWithoutFeedback>
        </View>
        :       
        <View style={styles.buttonContainer}>
        <TouchableWithoutFeedback>
            <Text style={[styles.TextButton, styles.buttonDisabled  ]}>Continuar</Text>
        </TouchableWithoutFeedback>
        </View>
          )
}


export default class LoginByCodeThirdStep extends Component {

    state = {
        validate: null,
        firstName: null,
        secondName: null,
        email: null
    }

    _onEndEditing = async text => {
        console.log(this._validate());
        
        
    }

    submitForm = () => {
        const { email, firstName, secondName } = this.state
        axios.post('http://localhost:3000/users', { email, firstName, secondName  });
    }

    goToSecondName= async  text => {
        await this.setState({ name: text })
        this.secondName.focus()//search is undefined
    }
  
    goToEmail = async text => {
      await this.setState({ secondName: text })
      this.email.focus()//search is undefined
      
    }

    _validate = () => {
        const { email, firstName, secondName } = this.state
        if(email && firstName && secondName) {
            return this.setState({ validate: true })    
        } 
            this.setState({ validate: false })  
    }

  render() {

    const { validate } = this.state

    return (
        <SafeAreaView>
      <View>
        <Text style={styles.title}>Agora falta pouco!</Text>

        <View style={ styles.form }>
        <TextInput 
        style={styles.textInputPhone} 
        placeholder="nome"
        returnKeyType="next"
        onEndEditing={this._onEndEditing} // quando finalizar o texto, bom para validação
        onSubmitEditing={this.goToSecondName} // bom pra proximo field pelo teclado
          ref={name => this.name = name}
        />
       { validate && <Text style={ styles.validateError }>O nome é obrigatório.</Text>}
        <TextInput 
        style={styles.textInputPhone} 
        returnKeyType="next"
        placeholder="Sobre nome"
        onEndEditing={this._onEndEditing} // quando finalizar o texto, bom para validação
        onSubmitEditing={this.goToEmail} // bom pra proximo field pelo teclado
ref={secondName => this.secondName = secondName}
        />
       { validate && <Text style={ styles.validateError }>O sobre nome é obrigatório.</Text>}

    <TextInput 
        style={styles.textInputPhone} 
        placeholder="email"
          ref={email => this.email = email}
          returnKeyType="done"

          autoComplete="email"
        />
        { validate && <Text style={ styles.validateError }>O email é obrigatório.</Text> }

        </View>

        <ConfirmButton validate={ this.state.validate }  submitForm={ this.submitForm }/>
      </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    buttonContainer: {
      top: 110
    },
    TextButton: {
      paddingVertical: 20,
      color: '#fff',
      fontFamily: fonts.ralewayBold,
      fontSize: 18,
      textAlign: 'center',
      marginHorizontal: 20,
    },
  
    buttonDisabled: {
      backgroundColor: '#9F92AD'
    },
  
    buttonEnabled: {
      backgroundColor: '#5C2173',
    },
  
    form: {
      justifyContent: 'space-between',
      alignSelf: 'center',
      top: 70,
      width: '90%',
    },
  
    textInputPhone: {
      fontSize: 20,
      backgroundColor: '#F0F2F1',
      paddingHorizontal: 25,
      paddingVertical: 15,
      marginVertical: 10,
      borderRadius: 6
      },
  
    title: {
      fontFamily: fonts.ralewayRegular,
      fontSize: 35,
      alignSelf: 'center',
      top: 40
    },
    'title-primary': {
      color: '#5C2173',
    },
  
    terms: {
      top: 400,
      alignSelf: 'center',
      fontSize: 4,
      fontFamily: fonts.ralewayRegular
    },

    validateError: {
       color: '#F5A522',
       
    }
  
  })
