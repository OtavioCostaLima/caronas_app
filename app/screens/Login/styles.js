import { StyleSheet } from 'react-native'
import { metrics, colors, fonts } from '../../styles'

const styles = new StyleSheet.create({

 container: {
     backgroundColor: colors.primary
 },

 textInput: { 
    borderRadius: 6, 
    marginLeft: 10,
    marginRight: 60, 
    fontSize: 20, 
    marginVertical: 5,
    backgroundColor: colors.white,
    fontFamily: fonts.ralewayLight 
},


})

export default styles;