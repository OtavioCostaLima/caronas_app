import React, { Component } from 'react'
import { Text, View, TouchableWithoutFeedback  } from 'react-native'
import styles from './styles'
import { Input } from 'react-native-elements';
import BackIcon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
import { SocialIcon } from 'react-native-elements'
import FBLoginButton from '../../components/Buttons/FBLoginButton/FBLoginButton'

export default class Login extends Component {

  setpPassword = () => {
      this._inputPassword.focus()
  }

  render() {
    

    saveUser = async (data) => {
      
    }


    return (

     <View style={{ flex: 1, alignItems: 'center' }}>
      <TouchableWithoutFeedback onPress={ () => this.props.navigation.navigate('Home') }>
           <BackIcon name="ios-arrow-back" size={ 35 }/>
      </TouchableWithoutFeedback>
<Input
  onSubmitEditing={ this.setpPassword }  returnKeyType="next"
  leftIcon={
    <Icon name='user' size={24} color='black'/>
  }
  placeholder='Usuário'
/>
<Input
  ref={ component => this._inputPassword = component }
  secureTextEntry={true}  
  leftIcon={
    <Icon name='lock' size={24} color='black'/>
  } placeholder='Password'
/>

   <View style={{ position: 'absolute', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', bottom: '30%', paddingHorizontal: 20 }}>
    <SocialIcon style={{ height: 68, width: 68, borderRadius: 50 }} onPress={() => {}} button type='facebook'/>
    
    <SocialIcon style={{ height: 68, width: 68, borderRadius: 50 }} onPress={() => {}} button light type='instagram'/>
    
    <SocialIcon style={{ height: 68, width: 68, borderRadius: 50 }} onPress={() => {}} button light type='google'/>

  </View>

  <FBLoginButton style={{ position:'absolute', backgroundColor: 'red', width: '80%', paddingHorizontal: '10' }}></FBLoginButton>

  <Text>Registrar</Text>

</View>
    )
  }
}
