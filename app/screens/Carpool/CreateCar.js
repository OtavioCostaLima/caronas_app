import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableWithoutFeedback, TextInput, 
  KeyboardAvoidingView, ScrollView } from 'react-native'
import { fonts } from '../../styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import axios from 'axios';
import ModalBrandCar from './ModalBrandCar'
import ModalCarModel from './ModalCarModel'
import ModalYearCar from './ModalYearCar'
import ModalColorCar from './ModalColorCar'
import ButtonConfirm from '../../components/Buttons/ButtonConfirm'

const ButtonModal = ({ onPress, value }) => (
  <TouchableWithoutFeedback onPress={onPress}>
    <View style={styles.containerStyle}>
      <TextInput style={styles.inputStyle} editable={false} value={value} />
      <Icon name="arrow-drop-down" size={40} color="#4D375B" />
    </View>
  </TouchableWithoutFeedback>
)

export default class CreateCar extends Component {

  constructor(props) {
    super(props)
  }

  state = {
    marcaSelected: { codigo: '', nome: '' },
    modeloSelected: {},
    anoSelected: {},
    colorSelected: {},
    modalMarcaVisible: false,
    modalModeloVisible: false,
    modalAnoVisible: false,
    modalCorVisible: false,

  }

  //closeModal = modal => this.setState({ ...modal })

  async getMarca(tipo = 'carros') {
    return await axios.get('https://parallelum.com.br/fipe/api/v1/' + tipo + '/marcas')
  }

  async getModelo() {
    const codigoCarro = this.state.marcaSelected.codigo
    return await axios.get('https://parallelum.com.br/fipe/api/v1/carros/marcas/' + codigoCarro + '/modelos')
  }

  async getAno() {
    const codigoCarro = this.state.marcaSelected.codigo
    const codigoModelo = this.state.modeloSelected.codigo

    return await axios.get('https://parallelum.com.br/fipe/api/v1/carros/marcas/' + codigoCarro + '/modelos/' + codigoModelo + '/anos')
  }

  _marcaSelected = item => this.setState({ marcaSelected: item })

  _modeloSelected = item => this.setState({ modeloSelected: item })

  _anoSelected = item => this.setState({ anoSelected: item })

  _colorSelected = item => this.setState({ colorSelected: item })


  renderModalModel = () => {
    return (
      <ModalCarModel
        itemSelected={item => this._modeloSelected(item)}
        fetchData={() => this.getModelo()}
        closeModal={() => this.setState({ modalModeloVisible: false })}
        visible={this.state.modalModeloVisible} />
    )
  }

  renderAnoModel = () => (
    <ModalYearCar
      itemSelected={item => this._anoSelected(item)}
      fetchData={() => this.getAno()}
      closeModal={() => this.setState({ modalAnoVisible: false })}
      visible={this.state.modalAnoVisible} />
  )

  renderModalColor = () => (
    <ModalColorCar
      itemSelected={item => this._colorSelected(item)}
      closeModal={() => this.setState({ modalCorVisible: false })}
      visible={this.state.modalCorVisible} />
  )

  render() {

    const { marcaSelected, modeloSelected, anoSelected, colorSelected } = this.state

    return (
      <View style={styles.rootContainer}>
        <KeyboardAvoidingView
          enabled
          behavior="position">
          <ScrollView>
            <Text style={styles.title}> Adicione um carro antes de continuar! </Text>
            <ModalBrandCar
              itemSelected={item => this._marcaSelected(item)}
              fetchData={() => this.getMarca()}
              closeModal={() => this.setState({ modalMarcaVisible: false })}
              visible={this.state.modalMarcaVisible} />

            {marcaSelected.codigo != 0 && this.renderModalModel()}

            {marcaSelected.codigo != 0 && this.renderAnoModel()}

            {this.renderModalColor()}

            <View style={styles.formContainer}>
              <Text style={styles.textStyle}>Marca</Text>
              <ButtonModal value={marcaSelected.nome} onPress={() => this.setState({ modalMarcaVisible: true })} />

              <Text style={styles.textStyle}>Modelo</Text>
              <ButtonModal value={modeloSelected.nome} onPress={() => this.setState({ modalModeloVisible: true })} />

              <Text style={styles.textStyle}>Ano</Text>
              <ButtonModal value={anoSelected.nome} onPress={() => this.setState({ modalAnoVisible: true })} />

              <Text style={styles.textStyle}>Cor</Text>
              <ButtonModal value={colorSelected.name} onPress={() => this.setState({ modalCorVisible: true })} />

              <Text style={styles.textStyle}>Placa</Text>
              <TextInput
                autoCapitalize='characters'
                style={styles.inputContainer} />
            </View>
          </ScrollView>
          <ButtonConfirm title="Confirmar" />
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1
  },
  formContainer: {
    marginHorizontal: 20
  },
  containerStyle: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#4D375B',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputStyle: {
    flex: 1,
    color: 'black',
    fontFamily: fonts.ralewayBold
  },

  textStyle: {
    fontFamily: fonts.ralewayBold,
    color: '#4D375B',
    marginTop: 10
  },
  inputContainer: {
    backgroundColor: '#F0F2F1',
    fontSize: 20,
    borderRadius: 5,
    marginBottom: 10,
    marginTop: 10
  },

  title: {
    fontFamily: fonts.ralewayRegular,
    fontSize: 20,
    alignSelf: 'center',
    marginVertical: 40
  },

})
