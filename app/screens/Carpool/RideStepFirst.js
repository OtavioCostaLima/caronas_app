import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, TouchableOpacity, Modal, TouchableWithoutFeedback } from 'react-native'
import DateInput from '../../components/DateInput'
import TimePicker from '../../components/TimePicker'
import { fonts } from '../../styles';
import { CheckBox } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons'
import ButtonConfirm from '../../components/Buttons/ButtonConfirm'
import GooglePlacesInput from '../../components/Inputs/GooglePlacesInput'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import * as TravelActions from '../../store/actions/travel'
import { colors } from '../../styles/theme';


const ModalLocation = ({ visible, onPress, onLocationSelected }) => (
        <Modal visible={visible}>
            <TouchableOpacity onPress={onPress} style={{ alignSelf: 'flex-end', top: 15 }}>
                <Icon name="close" size={40} />
            </TouchableOpacity>
            <GooglePlacesInput
                onLocationSelected={(data, details) => onLocationSelected(data, details)}
                style={[styles.inputContainer, { flex: 1 }]}
                placeholder="" />
        </Modal>
    )


class RideStepFirst extends Component {

    state = {
        checked: false,
        modalVisible: false,
        modalDestinationVisible: false,
        validateButtonModal: false,
        enabledButton: false,
        travel: {
            origin: {
                description: '',
                state: '',
                latitude: 0,
                longitude: 0,
            },
            destination: {
                description: '',
                state: '',
                latitude: 0,
                longitude: 0,
            },
            vacances: 1,
            dateLeft: '',
            timeLeft: '',
            dateReturn: '',
            timeReturn: ''
        }
    }

    addVacance = () => {

        if (this.state.travel.vacances < 4) {
            this.setState(prevState => ({
                travel: {
                    ...prevState.travel,
                    vacances: prevState.travel.vacances + 1
                }
            }))

        }
    }

    removeVacance = () => {
        if (this.state.travel.vacances > 1) {
            this.setState(prevState => ({
                travel: {
                    ...prevState.travel,
                    vacances: prevState.travel.vacances - 1
                }
            }))
        }

    }

    _onLocationOrigin = async (data, details) => {
       console.log('detalhes ', details);
       console.log('data ', data);

        console.log('address_components', details.address_components);

        const address_components = details.address_components;

        const city = address_components.filter(item => item.types.includes('administrative_area_level_2'))[0]
        const uf = address_components.filter(item => item.types.includes('administrative_area_level_1'))[0]
       console.log('city: ', city.long_name);
       console.log('uf: ', uf.short_name);

        await this.setState(prevState => ({
             travel: {
                ...prevState.travel,
                origin: {
                    description: details.formatted_address,
                    longitude: details.geometry.location.lng,
                    latitude: details.geometry.location.lat,
                }
            }
        }))
        this.setState({ modalVisible: false })
    }

    _onLocationDestination = async (data, details) => {
        console.log('detalhes ', details);

        await this.setState(prevState => ({
            travel: {
                ...prevState.travel,
                destination: {
                    description: details.formatted_address,
                    longitude: details.geometry.location.lng,
                    latitude: details.geometry.location.lat,
                }

            }
        }))
        this.setState({ modalDestinationVisible: false })
    }


    openModalTextInput1 = () => {
        this.setState({ modalVisible: true })

    }

    openModalTextInput2 = () => {
        this.setState({ modalDestinationVisible: true })
    }

    handleClickDatePicker = date => {
        this.setState({
            travel: { dateLeft: date }
        })
    }

    handleClickButton = () => {
        this.props.toggleAddTravel(this.state.travel)
        this.props.navigation.navigate('ScreenPrice')
    }

    render() {
       console.log('Props: ',this.props);
        //const travel = this.props.travel.travel
        const { enabledButton, modalVisible, travel, modalDestinationVisible } = this.state

        return (
            <View style={{ flex: 1 }}>
                <ModalLocation
                    onLocationSelected={(data, details) => this._onLocationOrigin(data, details)}
                    onPress={() => this.setState({ modalVisible: false })}
                    visible={modalVisible} />

                <ModalLocation
                    onLocationSelected={(data, details) => this._onLocationDestination(data, details)}
                    onPress={() => this.setState({ modalDestinationVisible: false })}
                    visible={modalDestinationVisible} />


                <View style={{ marginHorizontal: 10, marginTop: 30 }}>
                    <Text style={[styles.textStyle, { marginBottom: 5 }]}> Origem </Text>
                    <TouchableWithoutFeedback onPress={this.openModalTextInput1.bind(this)}>
                        <View>
                            <TextInput
                                value={travel.origin.description}
                                editable={false}
                                style={styles.inputContainer} placeholder="local de origem" />
                        </View>
                    </TouchableWithoutFeedback>


                    <Text style={[styles.textStyle, { marginTop: 20, marginBottom: 5 }]} > Destino </Text>
                    <TouchableWithoutFeedback onPress={this.openModalTextInput2}>
                        <View>
                            <TextInput 
                            value={travel.destination.description} 
                            editable={false}
                                style={styles.inputContainer} placeholder="local de destino" />
                        </View>
                    </TouchableWithoutFeedback>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                    <DateInput
                        containerStyle={styles.datePickerStyleContainer}
                        style={{ borderWidth: 0 }}
                        onClear={() => this.setState(
                            prevState => ({ travel: { ...prevState.travel, dateLeft: '' } }))}
                        handleClickDatePicker={date => this.setState(
                            prevState => ({ travel: { ...prevState.travel, dateLeft: date } }))
                        }
                        placeholder="Data da saída" />
                    <TimePicker
                        onClear={() => this.setState(
                            prevState => ({ travel: { ...prevState.travel, timeLeft: '' } }))}
                        handleClickTimePicker={date => this.setState(
                            prevState => ({ travel: { ...prevState.travel, timeLeft: date } }))
                        }
                        containerStyle={styles.datePickerStyleContainer}
                        placeholder="Hora saída" />
                </View>

                <CheckBox
                    textStyle={styles.textStyle}
                    checkedColor='#4D375B'
                    onPress={() => this.setState(prevState => ({ checked: !prevState.checked }))}
                    title='Vai voltar?'
                    checked={this.state.checked} />

                {this.state.checked &&
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                        <DateInput
                            containerStyle={styles.datePickerStyleContainer}
                            onClear={() => this.setState(
                                prevState => ({ travel: { ...prevState.travel, dateReturn: '' } }))}
                            handleClickDatePicker={date => this.setState(
                                prevState => ({ travel: { ...prevState.travel, dateReturn: date } }))
                            }
                            placeholder="Data da volta" />
                        <TimePicker
                            onClear={() => this.setState(
                                prevState => ({ travel: { ...prevState.travel, timeReturn: '' } }))}
                            handleClickTimePicker={date => this.setState(
                                prevState => ({ travel: { ...prevState.travel, timeReturn: date } }))
                            }
                            containerStyle={styles.datePickerStyleContainer}
                            placeholder="Hora volta" />
                    </View>
                }

                <View style={ styles.vacancesContainer }>
                    <Text style={[styles.textStyle, styles.vacancesText]}>Vagas</Text>

                    <TouchableOpacity onPress={this.removeVacance}>
                        <Icon style={styles.buttomDec} name="remove-circle-outline" size={40} color='#4D375B' />
                    </TouchableOpacity>

                    <Text style={[styles.textStyle, styles.vacanceNumber]}> {this.state.travel.vacances} </Text>

                    <TouchableOpacity onPress={this.addVacance}>
                        <Icon style={styles.buttomInc} name="add-circle-outline" size={40} color='#4D375B' />
                    </TouchableOpacity>
                </View>


                <View style={styles.bottom}>
                    <ButtonConfirm enabled={enabledButton} onPress={this.handleClickButton} />
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => ({ travel: state.travel })
const mapDispatchProps = dispatch => bindActionCreators(TravelActions, dispatch)

export default connect(mapStateToProps, mapDispatchProps)(RideStepFirst)

const styles = StyleSheet.create({

    textStyle: {
        color:  colors.darkPrimary,
        fontFamily: fonts.ralewayBold,
    },
    inputContainer: {
        backgroundColor: '#F0F2F1',
        fontSize: 20,
        paddingVertical: 15

    },
    vacancesContainer: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginHorizontal: 10, 
        marginTop: 10
    },
    vacancesText: {
        flex: 1
    },
    vacanceNumber: {
        fontSize: 25,
        width: 34,
        textAlign: 'center',
    },
    datePickerStyleContainer: {
        borderWidth: 0,
        borderBottomWidth: 2,
        elevation: 0,
        borderRadius: 0,
        flex: 1
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 70
    },

})
