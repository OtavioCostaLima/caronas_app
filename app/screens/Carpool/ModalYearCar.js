import React, { Component } from 'react'
import {
    Text, StyleSheet, View, Modal,
    TouchableWithoutFeedback, TextInput, FlatList, TouchableHighlight
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { fonts } from '../../styles';

// props:  itemSelected: function, closeModal; function , fetchData: Promise, visible: true, false
export default class ModalCar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            filterData: []
        }
    }

    async get() {
        try {
            console.log('show ano');
            
            const data = await this.props.fetchData()
            console.log('dados ano: ', data);

            this.setState({ data: data.data, filterData: data.data })
        } catch (err) {
            console.log('erro ano: ', err);
        }
    }

    _search = async text => {
        const filter = this.state.data.filter(item => item.nome.includes(text))
        await this.setState({ filterData: filter })
    }

    _renderItem({ item }) {
        return (
            <TouchableHighlight underlayColor="#F0F2F1"
                onPress={() => {
                    this.props.itemSelected(item)
                    this.props.closeModal()
                }}>
                <Text style={{ fontSize: 20, paddingVertical: 10, borderBottomWidth: 1, borderBottomColor: '#F0F2F1', marginHorizontal: 10 }}
                    key={item.codigo}>{item.nome}
                </Text>
            </TouchableHighlight>
        )
    }

    render() {
        const { visible, closeModal } = this.props
        const { filterData } = this.state

        return (
            <Modal 
            onShow={ () => this.get() }
            visible={visible}>
                <View style={{ top: 10 }}>
                    <TouchableWithoutFeedback onPress={closeModal}>
                        <View style={styles.closeButtom}>
                            <Icon name="close" size={40} />
                        </View>
                    </TouchableWithoutFeedback>
                    <TextInput
                        //autoFocus={true}
                        style={styles.inputContainer}
                        onChangeText={(text) => this._search(text)} />
                    <FlatList
                        data={filterData}
                        renderItem={item => this._renderItem(item)} />
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    textStyle: {
        fontFamily: fonts.ralewayBold,
        color: '#4D375B',
        marginTop: 10
    },
    inputContainer: {
        backgroundColor: '#F0F2F1',
        fontSize: 20,
        marginHorizontal: 10,
        borderRadius: 5,
        marginBottom: 10
    },
    closeButtom: {
        alignSelf: 'flex-end',
    }

})
