import React, { Component } from 'react'
import {
    Text, StyleSheet, View, Modal,
    TouchableWithoutFeedback, TextInput, FlatList, TouchableHighlight
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { fonts } from '../../styles';

const colors = [
    { name: 'black', rgb: '#fff'},
    { name: 'red', rgb: 'red'},
    { name: 'yellow', rgb: 'yellow'},
    { name: 'green', rgb: 'green'},
]

// props:  itemSelected: function, closeModal; function , fetchData: Promise, visible: true, false
export default class ModalCar extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    _renderColor(color) {
        return (
            <View style={{ backgroundColor: color, height: 50, width: 50, borderRadius: 40, borderWidth: 1, borderColor: '#F0F2F1', marginLeft: 10 }}></View>
        )
    }

    _renderItem({ item }) {
        return (
            <TouchableHighlight underlayColor="#F0F2F1"
                onPress={() => {
                     this.props.itemSelected(item)
                      this.props.closeModal()
                }}>
               <View style={{ alignItems:'center', flex:1, borderBottomWidth: 1, borderBottomColor: '#F0F2F1', flexDirection: 'row',  paddingVertical: 10, marginHorizontal: 10  }}>
               <Text style={{ fontSize: 20}}
                    key={item.name}>{item.name}
                </Text>
               { this._renderColor(item.rgb)}
               </View>

            </TouchableHighlight>
        )
    }

    render() {
        const { visible, closeModal } = this.props
        return (
            <Modal visible={visible}>
                <View style={{ top: 10 }}>
                    <TouchableWithoutFeedback onPress={closeModal}>
                        <View style={styles.closeButtom}>
                            <Icon name="close" size={40} />
                        </View>
                    </TouchableWithoutFeedback>
                    <FlatList
                        data={colors}
                        renderItem={item => this._renderItem(item)} />
                </View>
            </Modal>
        )
    }

}

const styles = StyleSheet.create({
    textStyle: {
        fontFamily: fonts.ralewayBold,
        color: '#4D375B',
        marginTop: 10
    },
    inputContainer: {
        backgroundColor: '#F0F2F1',
        fontSize: 20,
        marginHorizontal: 10,
        borderRadius: 5,
        marginBottom: 10
    },
    closeButtom: {
        alignSelf: 'flex-end',
    }

})
