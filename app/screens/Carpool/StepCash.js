import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput } from 'react-native'
import ButtonConfirm from '../../components/Buttons/ButtonConfirm'
import { connect } from 'react-redux'

import * as TravelActions from '../../store/actions/travel'
import { bindActionCreators } from 'redux';

class StepCash extends Component {

  state = { 
    price: 0
   }

  clickHandlleButton = () => {
    this.props.addPrice(this.state.price)
    this.props.navigation.navigate('DetailsCarpool')
  }


  render() {
    return (
      <View style={styles.rootContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}> Qual valor será cobrado </Text>
          <Text style={styles.title}> por passageiro? </Text>
        </View>

        <View style={styles.priceContainer}>
        <Text>R$</Text>
          <TextInput
            onChangeText={ text => this.setState({ price: text }) }
            style={styles.priceText}
            keyboardType="numeric"
            placeholder="30,00" />
        </View>

        <View style={styles.bottom}>
          <ButtonConfirm 
          onPress={ this.clickHandlleButton } 
          enabled={false} />
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({ price: state.travel.price })
const mapDispatchProps = dispatch => bindActionCreators(TravelActions, dispatch)

export default connect(mapStateToProps, mapDispatchProps)(StepCash)

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 40
  },

  priceContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    top: 150
  },

  priceText: {
    fontSize: 80
  },

  title: {
    fontSize: 30,
    alignSelf: 'center'
  },

  titleContainer: {
    top: 25,
    alignSelf: 'center',
    justifyContent: 'center',
  }



})
