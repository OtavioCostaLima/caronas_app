import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import ButtonConfirm from '../../components/Buttons/ButtonConfirm';
import { fonts } from '../../styles/theme'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

class DetailsCarpool extends Component {

  render() {
    const carpool = this.props.travel

    return (
      <View style={styles.rootContainer}>

        <Text style={{
          alignSelf: 'center',
          fontSize: 30,
          top: 20,
          marginBottom: 30,
          color: '#fff'
        }}>Está tudo certo?</Text>


        <View style={styles.card}>
          <View style={styles.localizationContainer}>
            <Text style={styles.textLocalization}> {carpool.origin.state} </Text>
            <Text style={styles.textCard}> {carpool.origin.description} </Text>
          </View>
        </View>

        <View style={styles.card}>
          <View style={styles.localizationContainer}>
            <Text style={styles.textLocalization}> {carpool.destination.state} </Text>
            <Text style={styles.textCard}> {carpool.destination.description} </Text>
          </View>
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center' }}>
          <View style={{ backgroundColor: '#F0F2F1', padding: 10, borderRadius: 4, width: 170, margin: 10 }}>
            <Text style={{ fontSize: 10 }}>ida</Text>
            <Text style={{ fontSize: 18 }}>{carpool.dateLeft}</Text>
            <Text style={{ fontSize: 18 }}>{carpool.timeLeft}</Text>
          </View>

          <FontAwesome name="calendar" size={25} />

          <View style={{ backgroundColor: '#F0F2F1', padding: 10, borderRadius: 4, width: 170, margin: 10 }}>
            <Text style={{ fontSize: 10 }}>retorno</Text>
            <Text style={{ fontSize: 18 }}>{carpool.dateReturn}</Text>
            <Text style={{ fontSize: 18 }}>{carpool.timeReturn}</Text>
          </View>
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10, justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 18 }} >{carpool.vacances} vaga(s)</Text>
          <Text style={{ fontSize: 30 }} >R$ {carpool.price}</Text>
        </View>


        <View style={styles.bottom}>
          <ButtonConfirm containerStyle={{ backgroundColor: '#fff' }} outline={true} />
        </View>

      </View>







    )
  }
}

export default connect(state => ({ travel: state.travel }))(DetailsCarpool)

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
  },
  card: {
    marginHorizontal: 10,
    borderRadius: 4,
    marginVertical: 10,
    borderLeftWidth: 4,
    borderLeftColor: '#4D375B',

  },
  textCard: {
    color: '#4D375B',
    fontFamily: fonts.regular,
    fontSize: 15,
  },
  itemCard: {
    //   backgroundColor: '#4D375B'
  },

  textLocalization: {
    //backgroundColor: '#4D375B',
    color: '#4D375B',
    fontSize: 28,


  },
  localizationContainer: {
    //flexDirection: 'row'
  },

  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 70
  },



})
