import React, { Component } from 'react'
import { AsyncStorage, Text, View, Image, StyleSheet, ScrollView, TouchableWithoutFeedback} from 'react-native'
import { AirbnbRating, } from 'react-native-elements';
import { colors, fonts } from '../../styles'
import DogIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import ImagePicker from 'react-native-image-picker';

const Travels = () => (
  <View style= {{  marginTop: 10 }}>
    <View style={{ flexDirection: 'row', borderWidth: 1, borderBottomWidth: 0 , borderColor: colors.defaultGrey, justifyContent: 'space-around' }}>
      <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Como Motorista</Text>
      <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Como Passageiro</Text>
    </View>
    <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: colors.defaultGrey, justifyContent: 'space-around' }}>
      <Text style={{ fontSize: 40 }}>22</Text>
      <Text style={{ fontSize: 40 }}>10</Text>
    </View>
  </View>
)

export default class index extends Component {
  static navigationOptions ={
  //  title: 'Perfil',
    headerTitleStyle: { textAlign: 'center', flex: 1 },
    headerStyle:{
      elevation: 0,
      shadowOpacity: 0,
    },
  }

  state = {
    avatarSource: '',
    name: ''
  }

 async componentDidMount() {
    this.setState({
      avatarSource: {uri: await AsyncStorage.getItem('imagemProfile')},
      name: await AsyncStorage.getItem('userName')
    })
  }

selectImage = () => {

  // More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
  title: 'Selecionar Foto',
  takePhotoButtonTitle: 'Tirar uma Foto',
  chooseFromLibraryButtonTitle: 'Galeria',
  cancelButtonTitle: 'Cancelar',
  mediaType: 'photo',
  cameraType: 'front',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

/**
 * The first arg is the options object for customization (it can also be null or omitted for default options),
 * The second arg is the callback which sends object: response (more info in the API Reference)
 */
ImagePicker.showImagePicker(options, (response) => {
  console.log('Response = ', response);

  if (response.didCancel) {
    console.log('User cancelled image picker');
  } else if (response.error) {
    console.log('ImagePicker Error: ', response.error);
  } else if (response.customButton) {
    console.log('User tapped custom button: ', response.customButton);
  } else {
    const source = { uri: response.uri };
      console.log(source);
      
    // You can also display the image using data:
    // const source = { uri: 'data:image/jpeg;base64,' + response.data };

    this.setState({
      avatarSource: source,
    });
  }
});


}
static navigationOptions = {
  title: 'Perfil'
}

  render() {


    
   return (
      <ScrollView style= {styles.root}>
      <TouchableWithoutFeedback onPress={  this.selectImage } >
        <Image style= {styles.imageProfile} source={ this.state.avatarSource
          || require('../../images/person1.jpg')
         } />
      </TouchableWithoutFeedback>
        <Text style= {styles.nameProfile}> {this.state.name} </Text>
          <AirbnbRating readonly count={5} reviews={[]} size={20} />  
          
          <Text style={{ alignSelf: 'center' }} >Minhas Viagens</Text>
        

        <Travels/>

       <Text>Minhas Preferências</Text>

         <View style= {{  flexDirection: 'row', justifyContent: 'center' }}>
          <DogIcon style={ [styles.prefIcon, styles.dogIcon] } name="dog" size={40}/>
          <DogIcon style={ [styles.prefIcon, styles.smokingIcon] } name="smoking" size={40}/>
          <DogIcon style={ [styles.prefIcon, styles.musicIcon] } name="music" size={40}/>
         </View>
      </ScrollView>
    )
  }
}


const styles = StyleSheet.create({
  root: {
    flex: 1
  }, 
  imageProfile: {
    borderRadius: 100,
    height: 150,
    width: 150,
    alignSelf: 'center'
  },
  nameProfile: {
    fontSize: 20,
    alignSelf: 'center',
    color: colors.regular,
    fontFamily: fonts.ralewayBold,

  },
  prefIcon: {
    width: 40,
    color: colors.white,
    margin: 5,

  }, 
  dogIcon: {
    backgroundColor: colors.defaultGreen,
  },
  smokingIcon: {
    backgroundColor: colors.defaultOrange,
  },
  musicIcon: {
    backgroundColor: colors.defaultPink,
},
})
