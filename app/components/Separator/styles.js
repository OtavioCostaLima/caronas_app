import { StyleSheet } from 'react-native'

const styles = new StyleSheet.create ({

    separator: {
        paddingHorizontal: 10,
        borderBottomWidth: 0.9,
        borderColor: '#CED0D4',
        marginHorizontal: 4
    }


});


export default styles;