import { StyleSheet } from 'react-native'
import { metrics, colors, fonts } from '../../../styles'

const styles = new StyleSheet.create ({

container: {
    position: 'absolute',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    backgroundColor: colors.primary,
    width: "90%",
    elevation: 5,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowOffset: { x: 0, y:0 },
    shadowRadius: 15,
},

'button-outline': {
    backgroundColor: colors.white,
    fontFamily: fonts.ralewayBold,
    fontSize: fonts.big,
    borderColor: colors.background,
    borderWidth: 2
},

'text-outline': {
    color: colors.primary,
},

text: {
    color: colors.white,
    fontFamily: fonts.ralewayBold,
    fontSize: fonts.big,
    textAlign: 'center',
    paddingVertical: 10,
},


})

export default styles;