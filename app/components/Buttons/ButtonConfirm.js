import React, { Component } from 'react'
import { Text, StyleSheet, TouchableHighlight } from 'react-native'
import { fonts } from '../../styles';


const ButtonConfirm = ({ containerStyle, titleStyle, type,
    title = 'Confirmar', onPress, enabled = true, outline = false }) => {
    return (
        <TouchableHighlight
            onPress={onPress}
            style={[
                styles.buttonContainer,
                outline && styles.outlineStyle,
                containerStyle,
                //   enabled ? styles.buttonEnabled : styles.buttonDisabled

            ]}
        >
            <Text style={[
                styles.titleStyle, 
            titleStyle,
            outline && styles.textOutlineStyle, 
            ]}>{title}</Text>
        </TouchableHighlight>
    )
}

export default ButtonConfirm

const styles = StyleSheet.create({

    buttonContainer: {
        marginVertical: 5,
        backgroundColor: '#5C2173',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        width: '90%'
    },

    outlineStyle: {
        color: '#5C2173',
        borderColor: '#5C2173',
        borderWidth: 2

    },

    textOutlineStyle: {
 

        color: '#5C2173',

    },

    titleStyle: {
        fontSize: 18,

        color: '#fff',
        fontFamily: fonts.ralewayBold,
    },

    buttonDisabled: {
        backgroundColor: '#9F92AD'
    },
    buttonEnabled: {
        backgroundColor: '#5C2173',
    },

})
