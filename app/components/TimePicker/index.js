import React, { Component } from 'react'
import { TouchableOpacity, TextInput, View, TimePickerAndroid } from 'react-native'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'


export default class DatePicker extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    time: null,
    leftIconVisible: false,
  }

  getDateDatePicker = async () => {
    this.inputText.blur()
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        // hour: hour,
        // minute: minute,
        is24Hour: false, // Will display '2 PM'
        mode: 'spinner'
      });

      if (action !== TimePickerAndroid.dismissedAction) {
        const time = (hour + 'h:' + minute + 'm')
        this.setState({
          time: time,
          leftIconVisible: true
        })
        this.props.handleClickTimePicker(hour + ':' + minute)
      }
    } catch ({ code, message }) {
      alert(error)
    }
  }

  clearDate = (time) => time && this.setState({ time: null, leftIconVisible: false })

  render() {
    const { style, placeholder, type, containerStyle } = this.props
    const { time, leftIconVisible } = this.state

    return (
      <View style={[styles.containerDateInput, containerStyle]}>
        <TouchableOpacity onPress={this.getDateDatePicker}>
          <Icon style={[styles.icon]} name="update" size={22} />
        </TouchableOpacity>
        <TextInput
          ref={inputText => this.inputText = inputText}
          type={type} value={time} placeholder={placeholder} onFocus={this.getDateDatePicker} style={[styles.inputText, style]} />

        {leftIconVisible &&
          <TouchableOpacity onPress={this.clearDate}>
            <Icon style={[styles[`right-icon`]]} name="close" size={22} />
          </TouchableOpacity>
        }
      </View>
    )
  }
}