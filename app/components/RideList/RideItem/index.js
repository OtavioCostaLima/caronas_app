import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './styles'
import Button from '../../Button'
import Separator from '../../Separator'

const Ride = ({ ride , navigation}) => (

    <View style={styles.container}>
        <View style={ styles.personContainer }>
            <Image style={ styles.image } source={{uri: ride.image }} />
            <Text style={ styles.personName }>{ ride.name }</Text>
            <Text>{ ride.type } Carona</Text>
        </View>

        <View style= { styles.content }>
            <Text style={ styles.title }>{ ride.origin } x { ride.destination }</Text>
            <Text style={ styles.leave } >Saindo { ride.ride_data } às { ride.leave_at }</Text>
            <Text style={ styles.leave } >Vagas: { ride.available_vacancies }</Text>
            <Text style={ styles.leave } >R$ { ride.amount }</Text>
        </View>

        <Separator/>

    <View style={ styles.containerButton }>
        <Button onPress={ () => navigation.navigate('SearchRide') }   style={ styles.butonRide } >Quero</Button>
        <Button style={ styles.butonRide } >Mais detalhes</Button>
    </View>

    </View>
);

export default Ride;