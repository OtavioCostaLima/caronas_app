import { StyleSheet } from 'react-native'
import { metrics, colors, fonts } from '../../../styles'

const styles = new StyleSheet.create ({

    container: {
        backgroundColor: colors.white,
        borderRadius: 3,
        marginBottom: 2,
        shadowColor: colors.light
    },

    containerButton: {
        flex: 1,
        flexDirection: 'row',
    },

    butonRide: {
        fontSize: fonts.regular,
        fontWeight: 'bold',
        color: colors.primary
    },

    image: {
        width: 60, 
        height: 60,
        borderRadius: 50
    },

    personContainer: {
     //   backgroundColor: '#f3f3f3',
        flexDirection: 'row',
        paddingHorizontal: 10,
        padding: 10,
    },

    personName: {
        fontSize: 18,
        color: colors.regular,
        paddingHorizontal: 10,
        paddingTop: 10
    },

    content: {
        padding: 30,
    },

    title: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    },

    leave: {
        textAlign: 'center',
        fontWeight: 'bold',

    }




});


export default styles;