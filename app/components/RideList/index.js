import React, { Component } from 'react'
import { View } from 'react-native'
import RideItem from './RideItem'
import rides from '../../apiTest'

export default class index extends Component {

    state = { rides }

    render() {

        const ridesFilters = this.state.rides.filter(ride => ride.type == this.props.filters.type )

        return (
            <View style={ backgroundColor='red' }>
                 {ridesFilters.map(ride => <RideItem key={ ride.id } ride={ ride } navigation={ this.props.navigation } />) }
             </View>
        );
    }
}