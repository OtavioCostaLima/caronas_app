import React from 'react'
import MapViewDirectons from "react-native-maps-directions";

const Directions = ({ destination, origin, onReady }) => (
<MapViewDirectons
    destination={ destination }
    origin = { origin }
    onReady={onReady}
    apikey="AIzaSyBnc7_-Tp8bZ9NxsYXH0GIZ5DMX0tx4UdQ"
    strokeWidth={3}
    strokeColor="#222"
/>
)

export default Directions