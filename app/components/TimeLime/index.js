import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'


export default class TimeLine extends Component {
  render() {
    return (
        <View style={{ alignItems: 'center', justifyContent: 'space-between', height: 80 }}>
            <View  style= { styles.circle } />
            <View  style= { styles.circleMd } />
            <View  style= { styles.circleMd } />
            <View  style= { styles.circleMd } />
            <View  style= { styles.circle } />
        </View>
    )
  }

}



const styles = new StyleSheet.create({
    circle: {
        height: 16,
        width: 16,
        borderRadius: 50,
        borderColor: "#86BA69",
        borderWidth: 3,

    },

    circleMd: {
        height: 8,
        width: 8,
        borderRadius: 50,
        borderColor: "#86BA69",
        borderWidth: 2,

    }
})
