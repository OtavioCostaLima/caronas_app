import React, { Component } from 'react'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Platform, StyleSheet } from 'react-native'

export default class GooglePlacesInput extends Component {

  state = {
    SearchFocused: false,
  }

  render() {

    const { SearchFocused } = this.state
    const { onLocationSelected } = this.props

    return (
      <GooglePlacesAutocomplete
        query={{
          key: 'AIzaSyCIXUX8Aiho1CxUIdGW7XSz0EbLagBxHls',
          language: 'pt',
          components: 'country:br'
        }}
        onPress={(data, details) => onLocationSelected(data, details)}
        autoFocus={true}
        currentLocation={true}
        renderDescription={row => row.description}
        currentLocationLabel="Localização atual"
        placeholder="Para onde?"
        placeholderTextColor="#333"
        textInputProps={{
          onFocus: () => { this.setState({ SearchFocused: true }) },
          onBlur: () => { this.setState({ SearchFocused: false }) },
          autoCapitalize: "none",
          autoCorrect: false
        }}
        listViewDisplayed={SearchFocused}
        fetchDetails={true}
        enablePoweredByContainer={false}
        styles={styles}
      />
    )
  }
}


const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: Platform.select({ ios: 80, android: 60 }),
    width: "100%"
  },
  textInputContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    height: 54,
    marginHorizontal: 10,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  textInput: {
    height: 54,
    margin: 0,
    borderRadius: 0,
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    fontSize: 25,
    backgroundColor: '#F0F2F1',

  },
  listView: {
    borderWidth: 1,
    borderColor: '#DDD',
    backgroundColor: '#FFF',
    marginHorizontal: 20,
    //marginTop: 10
  },
  description: {
    fontSize: 16
  },
  row: {
    padding: 20,
    height: 58
  }
})