
import React from 'react'
import { TextInput }   from 'react-native'
import styles from './styles'

const Input = ({ style, value, placeholder, onFocus }) => (
    <TextInput value={value} placeholder={placeholder} onFocus={onFocus} style={ [styles.textInput, style] }/>
)

export default Input