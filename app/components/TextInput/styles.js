import { StyleSheet } from 'react-native'
import { inputStyle } from '../../styles/components'

const styles = StyleSheet.create({
 
  textInput: { 
  ...inputStyle.container
},

})
export default styles
