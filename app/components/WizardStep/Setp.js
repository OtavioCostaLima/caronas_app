import React, { Component } from 'react'
import { View, Button } from 'react-native'

class Step extends Component {

    state = {}

    render() {
       return (
        <View style={{ flex: 1 }}>
            {this.props.children({ onChangeValue: this.props.onChangeValue, values: this.props.values })}
            <Button title="Prev" onPress={ this.props.prevStep } disabled={this.props.currentIndex == 0}/>
            { this.props.isLast 
            ? <Button title="Submit" onPress={ this.props.onSubmit } />
            : <Button title="Next" onPress={ this.props.nextStep } disabled={this.props.isLast}/>
            }
        </View>
    )
    }
}

    export default Step