import {StyleSheet} from 'react-native'
import { fonts, colors } from '../../styles'
import { inputStyle } from '../../styles/components'

const styles = StyleSheet.create({
    containerDateInput: {
        flexDirection: 'row',
        alignItems: 'center',
        ...inputStyle.container,
    },

    inputText: {
        ...inputStyle.inputText,
        position: 'relative',
    },

    icon: {
        paddingHorizontal: 10,
    },

    'right-icon': {
        paddingHorizontal: 10,
     }
})

export default styles