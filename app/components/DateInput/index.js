import React, { Component } from 'react'
import { View, TextInput, TouchableOpacity, DatePickerAndroid } from 'react-native'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'

class DateInput extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    date: null,
    leftIconVisible: false,
  }

  getDate = async () => {
    this.inputText.blur()
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date(),
        mode: 'spinner',
        minDate: new Date()
      });

      if (action !== DatePickerAndroid.dismissedAction) {
        const date = (day + '/' + month + '/' + year)
        this.setState({
          date: date,
          leftIconVisible: true
        })
        this.props.handleClickDatePicker(date)
      }
    } catch (error) {
      alert(error)
    }
  }

  clearDate = (date) => {
    date && this.setState({ date: null, leftIconVisible: false })
    this.props.onClear()
  }

  render() {
    const { style, placeholder, type, containerStyle } = this.props
    const { date, leftIconVisible } = this.state

    return (
      <View style={[styles.containerDateInput, containerStyle]}>
        <TouchableOpacity onPress={this.getDate}>
          <Icon style={[styles.icon]} name="date-range" size={22} />
        </TouchableOpacity>
        <TextInput
          ref={inputText => this.inputText = inputText}
          type={type}
          value={date}
          placeholder={placeholder}
          onFocus={this.getDate}
          style={[styles.inputText, style]}
        />

        {leftIconVisible &&
          <TouchableOpacity onPress={this.clearDate}>
            <Icon style={[styles[`right-icon`]]} name="close" size={22} />
          </TouchableOpacity>
        }
      </View>
    )
  }

}

export default DateInput
