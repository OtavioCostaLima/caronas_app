import React, { Component } from 'react';
import { StyleSheet, View, Text, Alert} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Search from '../../components/Map/Search'
import Directions from '../../components/Directions'
import { getPixelSize }  from '../utils'

export default class Map extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        region: null,
        error: null,
        destination: null
    };

    async componentDidMount () {
        navigator.geolocation.getCurrentPosition( 
            ({ coords: { latitude, longitude }}) => {
                this.setState({ 
                    region: { 
                        latitude, 
                        longitude, 
                        latitudeDelta: 0.0143,
                        longitudeDelta: 0.0134
                    }
                 });
            }, //sucesso
            (error) => {  
                this.setState({
                    error: error.message 
                })}, //erro
            {
                timeout: 1000,
                enableHighAccuracy: true,
            }
        );
    
    }

    handleonLocationSelected = (data, { geometry }) => {
        const { location: { lat: latitude, lng: longitude } } = geometry
        
        this.setState({
            destination: {
                latitude,
                longitude,
                title: data.structured_formatting.main_text,
            }
        })
    
    }

    render() {
        const { region, destination, error } = this.state;

        return (
            <View style={{ flex: 1}}>
            <MapView style={{ flex: 1}}
                showsUserLocation={ true }
                provider={PROVIDER_GOOGLE}
            region={ region }
            loadingEnabled
            showsMyLocationButton={true}
            ref= { el => this.MapView = el }
            >
            
            {
                destination && (
                <Directions 
                origin={region}
                destination={destination}
                onReady={ result => {
                    this.MapView.fitToCoordinates(result.coordinates, {
                        edgePadding: {
                            right: getPixelSize(50),
                            left: getPixelSize(50),
                            top:getPixelSize(50),
                            bottom: getPixelSize(50),
                        }})
                }}
                />
            ) }
            
            </MapView>
             <Search onLocationSelected={ this.handleonLocationSelected } />
        </View>

        );
    }
}