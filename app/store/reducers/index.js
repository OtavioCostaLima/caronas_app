import { combineReducers } from 'redux'
import travel from './travel'


export default combineReducers({
    travel
})