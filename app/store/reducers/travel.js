import { TOGGLE_ADD_TRAVEL, ADD_PRICE_TRAVEL } from '../actions/types'

const INITIAL_STATE = {
    origin: {
        description: 'Rua Paulo Ramos, 811',
        state: 'Barão de Grajaú Maranhão',
        latitude: 0,
        longitude: 0,
    },
    destination: {
        description: 'Dom José Freire Falcão, 3096',
        state: 'Teresina PI',
        latitude: 0,
        longitude: 0,
    },
    vacances: 1,
    dateLeft: '02/10/2019',
    timeLeft: '12:00 pm',
    dateReturn: '02/10/2019',
    timeReturn: '12:00 pm',
    price: '30,00'
}

export default function reducer(state = INITIAL_STATE, acttion) {
    switch(acttion.type) {
        case TOGGLE_ADD_TRAVEL:
        console.log('entrou na ação', acttion);
        return { ...state,  ...acttion.payload }
        case ADD_PRICE_TRAVEL: 
        return { ...state, price: acttion.payload }
    }
    return state
}