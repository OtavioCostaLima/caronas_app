import { TOGGLE_ADD_TRAVEL, ADD_PRICE_TRAVEL } from './types'
import axios from 'axios';

import { baseUrlApiCaronas } from '../../config/utils'

export function toggleAddTravel(travel) {
     return {
        type: TOGGLE_ADD_TRAVEL,
        payload: travel
    }
}

export function addPrice (price) {
    return {
        type: ADD_PRICE_TRAVEL,
        payload: price
    }
}

export function createTravel (travel) {
    return (dispatch) => {
        axios.post(`${baseUrlApiCaronas}/carpool`, { ...travel }).then(response => {
            dispatch(createTravelSucess(response.data))
        }).catch(error => {
            console.log('dispatch error', error);
        })
    }
}

export function createTravelSucess (data) {
    return {
        type: 'CREATE_POST_SUCESS',
        payload: data
    }
}