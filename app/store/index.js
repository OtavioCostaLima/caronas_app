import { createStore, applyMiddleware, compose  } from 'redux'
import rootReducer from './reducers'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'remote-redux-devtools'

const composeEnhancers = composeWithDevTools({
  realtime: true,
  hostname: '192.168.0.7',
  port: 8000
})

const store = createStore(
  rootReducer, 
  composeEnhancers(applyMiddleware(thunk))
  )

export default store