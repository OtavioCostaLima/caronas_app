import React, {Component} from 'react';
import Routes from './navigations'
import { Provider } from 'react-redux'
// import store from './store/'
import store from './store';

console.disableYellowBox = true;


export default class App extends Component {

  render() {
    return (
      <Provider store={store}>
          <Routes/>
      </Provider>
    );
  }

}


