const colors = {
    primary: '#5C2173', // default
    regular: '#521D66', // default

    // primary: '#1A73E8',
    background: '#f3f3f3',

    primary2: '#57E6BB',
    primary3: '#339B7F',
    darker: '#111',
    dark: '#333',
    //regular: '#666',
    light: '#C0C0C0',

    white: '#FFF',

    defaultGreen: '#86BA69', 
    defaultOrange: '#F5922F', 
    defaultPink: '#FB607E',
    defaultBlue: '#3F51B5',
    
    defaultGrey: '#f3f3f3',

    transparent: 'rgba(0, 0, 0, 0)',


    // colors for ripple
    // #DCD3D1 from #f3f3f3
    //#5C2173 from #521D66


}


export default colors;