const fonts = {
    input: 16,
    tiny: 10,
    small: 11,
    medium: 12,
    regular: 14,
    big: 20,
    
    ralewayBold: 'raleway-bold',
    ralewayExtrabold: 'raleway-extrabold',
    ralewayExtralight: 'raleway-extralight',
    ralewayHeavy: 'raleway-heavy',
    ralewayLight: 'raleway-light',
    ralewayMedium: 'raleway-medium',
    ralewayRegular: 'raleway-regular',
    ralewaySemibold: 'raleway-semibold',
    ralewayThin: 'raleway-thin',
  };
  
  export default fonts;