import { StyleSheet } from 'react-native'
import { fonts, colors } from '../index'

const inputStyle = StyleSheet.create({

  container: { 
    borderRadius: 6, 
    marginLeft: 10,
    marginRight: 10, 
    fontSize: 20, 
    marginVertical: 5,
    //backgroundColor: colors.white,
    elevation: 5,
    fontFamily: fonts.ralewayLight,
    borderWidth: 2,
    borderColor: colors.primary  
},

inputText: {
    flex: 1
}

})
export default inputStyle