import { createSwitchNavigator, createAppContainer } from 'react-navigation'

import CreateCars from '../screens/Carpool/CreateCar'
import CarpoolNavigation from './carpool'
import LoginNavigation from './login'
import MenuNavigation from './menu'

const Routes = createAppContainer(
  createSwitchNavigator({
    LoginNavigation,
    CarpoolNavigation,
    CreateCars, // teste
    MenuNavigation,
  }, {
      initialRouteName: "LoginNavigation"
    })
);

export default Routes;
