import { createStackNavigator } from 'react-navigation'
import Menu from '../screens/Menu'
import Profile from '../screens/Profile'
import About from '../screens/About'
import Help from '../screens/Help'
import Dashboard from '../screens/Dashboard/Dashboard'

export default createStackNavigator({
    Dashboard,
    Menu,
    Profile,
    About,
    Help
  }, {
    initialRouteName: "Dashboard",
  })