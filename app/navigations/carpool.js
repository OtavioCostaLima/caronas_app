import { createStackNavigator } from 'react-navigation'
import RideStepFirst from '../screens/Carpool/RideStepFirst'
import ScreenPrice from '../screens/Carpool/StepCash'
import DetailsCarpool from '../screens/Carpool/DetailsCarpool'

export default createStackNavigator({
    RideStepFirst,
    ScreenPrice,
    DetailsCarpool
}, {
    headerMode: 'none',
    initialRouteName: 'RideStepFirst'
})