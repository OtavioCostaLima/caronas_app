import { createStackNavigator } from 'react-navigation'
import LoginByCodeFirstStep from '../screens/Login/LoginByCode/LoginByCodeFirstStep'
import LoginByCodeSecondStep from '../screens/Login/LoginByCode/LoginByCodeSecondStep'
import LoginByCodeThirdStep from '../screens/Login/LoginByCode/LoginByCodeThirdStep'

export default createStackNavigator({
    LoginByCodeFirstStep,
    LoginByCodeSecondStep,
    LoginByCodeThirdStep
},{
    headerMode: 'none',
    initialRouteName: "LoginByCodeFirstStep"
})