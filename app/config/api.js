import axios from 'axios'

const api = axios.create({
    baseURL: "http://localhost:300"
  });
  
  api.defaults.headers.common["authorization"] =
    "Basic YmlsbGIyMTEyOnl5ZXl5ZSQx";
  
  export default api;